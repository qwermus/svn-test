<?php

namespace src\Route;

/**
 * Class Router to connect route and method
 * @author Volodymyr Lvov
 * @date 17.11.2020
 */
class Router
{
    /**
     * @var bool [if some action was done]
     */
    private $done = false;

    /**
     * Register GET request
     *
     * @param string $uri
     * @param $func
     * @return bool
     */
    public function get(string $uri, callable $func) : bool
    {
        // If request method is not POST or URI is wrong
        return $this->checkMethodAndUri('GET', $uri, $func);
    }

    /**
     * Register POST request
     * We do not use other requests in test task
     *
     * @param string $uri
     * @param $func
     * @return bool
     */
    public function post(string $uri, callable $func) : bool
    {
        // If request method is not POST or URI is wrong
        return $this->checkMethodAndUri('POST', $uri, $func);
    }

    /**
     * Register POST request
     * We do not use other requests in test task
     *
     * @param string $uri
     * @param $func
     * @return bool
     */
    public function other(callable $func) : bool
    {
        // If some action was done yet
        if ($this->getDone() === true) {
            return false;
        }

        // Call the action
        $this->makeAction($func);
        return true;
    }

    /**
     * Check if method and uri are correct
     *
     * @param string $className
     * @return string
     */
    private function checkMethodAndUri(string $method, string $uri, callable $func) : bool
    {
        // If some action was done yet
        if ($this->getDone() === true) {
            return false;
        }

        // If request method is not GET
        if ($_SERVER['REQUEST_METHOD'] != $method) {
            return false;
        }

        // If uri is wrong
        if ($_SERVER['REQUEST_URI'] != $uri) {
            return false;
        }

        // Call the action
        $this->makeAction($func);
        return true;
    }

    /**
     * Make action
     *
     * @param callable $func
     */
    private function makeAction(callable $func)
    {
        // If ok - lets mark that function was done...
        $this->setDone();

        // ...and call the function
        $func();
    }

    /**
     * Mark that function was done. No more routing available
     */
    private function setDone()
    {
        $this->done = true;
    }

    /**
     * Get is function was done
     */
    private function getDone() : bool
    {
        return $this->done;
    }
}