<?php

namespace src\Controllers;

use src\Storage\ContentFile;
use src\View\Viewer;

class User extends Controller
{
    /**
     * Get action - show saved data and form
     */
    public function getAction()
    {
        // Call template
        (new Viewer())->render('user/get', [
            'savedData' => ContentFile::read(CONTENT_FILE),
            'error' => $_SESSION['error'] ?? null
        ]);

        // Unset session if it was setted
        unset($_SESSION['error']);
    }

    /**
     * Post action
     */
    public function postAction(array $request = [])
    {
        // Really it is better to move this to validation class
        // But for test task it is doesn't metter
        if (!isset($request['name']) || strlen($request['name']) < 2 || strlen($request['name']) > 100) {

            // Check name
            $_SESSION['error'] = 'Name is not correct';

        } else if (!isset($request['email']) || !preg_match("/^[-\w\.]+@([-a-z0-9]+\.)+[a-z]{2,4}$/i", $request['email'])) {

            // Check email
            $_SESSION['error'] = 'Email is not correct';

        } else {

            // Otherwise lets add data to file
            $data = ContentFile::addRow(CONTENT_FILE, [
                'name' => $request['name'],
                'email' => $request['email']
            ]);

            // And save file
            ContentFile::save(CONTENT_FILE, $data);
        }

        // And go back
        header('location: /get');
        die();
    }
}