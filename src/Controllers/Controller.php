<?php

namespace src\Controllers;

use src\View\Viewer;

/**
 * Abstract Class Controller - it will be extended by other controllers
 * @package src\Controllers
 * @author Volodymyr Lvov
 * @date 17.11.2020
 */
abstract class Controller
{
    /**
     * Controller has two abstract methods
     */
    abstract protected function getAction();
    abstract protected function postAction();

    /**
     * Common method to show 404
     */
    public function notFoundAction()
    {
        (new Viewer())->render('errors/404');
    }

}