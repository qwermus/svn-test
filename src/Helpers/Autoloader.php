<?php

/**
 * Class Autoloader to load other classes
 * @author Volodymyr Lvov
 * @date 17.11.2020
 */
final class Autoloader
{
    /**
     * Register method
     */
    public static function register()
    {
        spl_autoload_register(function ($className) {
            $fileName = self::getFile($className);
            if (is_file($fileName)) {
                require($fileName);
            }
        });
    }

    /**
     * Return a class file uri
     *
     * @param string $className
     * @return string
     */
    private static function getFile(string $className) : string
    {
        return str_replace(
            '\\',
            DIRECTORY_SEPARATOR,
            $className.'.php'
        );
    }
}