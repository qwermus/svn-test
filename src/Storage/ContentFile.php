<?php

namespace src\Storage;


/**
 * Class ContentFile to work with saved data
 * @author Volodymyr Lvov
 * @date 17.11.2020
 */
class ContentFile
{
    /**
     * Read a file
     *
     * @param string $filename
     * @return string|null
     */
    public static function read(string $filename) : ?array
    {
        // If there is no file
        if (!is_file($filename)) {
            return null;
        }

        // If can not read file
        if (!$content = file_get_contents($filename)) {
            return null;
        }

        // Return file content
        return self::decode($content);
    }

    /**
     * Save a file
     *
     * @param string $filename
     * @return string|null
     */
    public static function save(string $filename, string $data)
    {
        file_put_contents($filename, $data);
    }

    /**
     * Add row to file
     *
     * @param string $filename
     * @return string|null
     */
    public static function addRow(string $filename, array $data) : ?string
    {
        // Get file content
        if (is_null($content = self::read($filename))) {
            $content = [];
        }

        // Add one row
        $content[] = $data;

        // Save content
        return self::encode($content);
    }

    /**
     * Get data from JSON
     * It is better to move json logic to trait or other class and use factory
     * But for test it is ok
     *
     * @param string $string
     * @return mixed
     */
    public static function decode(string $string) : array
    {
        return json_decode($string, 1);
    }

    /**
     * Put data to JSON
     *
     * @param string $string
     * @return mixed
     */
    public static function encode(array $object) : string
    {
        return json_encode($object);
    }
}