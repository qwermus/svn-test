<?php

namespace src\View;

use Exception;

/**
 * Class Viewer for render tpl
 * @author Volodymyr Lvov
 * @date 17.11.2020
 */
class Viewer
{
    /**
     * @var string [views folder]
     */
    public $folder = VIEW_DIR;

    /**
     * @var string [views extension]
     */
    public $ext = '.tpl.php';

    /**
     * Render template
     *
     * @param string $fileName
     * @param array $parameters
     */
    public function render(string $fileName, array $parameters = [])
    {
        // Lets find a file
        if (is_null($fileName = $this->getFile($fileName))) {
            throw new Exception('Template file not found');
        }

        // Include file
        $this->incTemplate($fileName, $parameters);
    }

    /**
     * Lets find a template file
     *
     * @param string $fileName
     * @return string|null
     */
    private function getFile(string $fileName) : ?string
    {
        // Generate correct file name
        $fileName = $this->folder . $fileName . $this->ext;

        // Lets find a file
        if (is_file($fileName)) {
            return $fileName;
        }

        // If there is no file
        return null;
    }

    /**
     * Execute the template by extracting the variables into scope, and including
     * the template file.
     *
     * @internal param $template
     * @internal param $variables
     *
     * @return string
     */
    private function incTemplate(string $fileName, array $parameters)
    {
        // Get parameters
        foreach ($parameters as $key => $value) {
            $$key = $value;
        }

        // Show template file
        require($fileName);
    }
}