<?php

/**
 * Views folder
 */
define('VIEW_DIR', __DIR__ . '/views/');

/**
 * Where to save data
 */
define('CONTENT_FILE', __DIR__ . '/content_file.json');