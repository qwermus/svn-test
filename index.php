<?php

session_start();
require('config.php');

/**
 * Run autoloader
 */
require('src/Helpers/Autoloader.php');
Autoloader::register();

/**
 * Uses
 */
use src\Route\Router;
use src\Controllers\User;

/**
 * Run router
 */
$router = new Router();

// Get method
$router->get('/get', function(){
    User::getAction();
});

// Post method
$router->post('/post', function(){
    User::postAction($_POST);
});

// Default method
$router->other(function(){
    User::notFoundAction();
});