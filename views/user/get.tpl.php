<!--
    We don't use here headers, requires, bootstrap, javascript and so on, because it is test task!
    This view is very simple
-->
<html>
<body>

    <?php
    // Show error
    if (isset($error)) {
        echo '<h3>'.$error.'</h3>';
    }
    ?>

    <!-- Show form -->
    <h2>Form:</h2>
    <form method="post" action="/post">
        <p>
            Name: <input type="text" name="name" maxlength="100">
        </p>
        <p>
            Email: <input type="email" name="email" maxlength="100">
        </p>
        <p>
            <input type="submit" value="Send data">
        </p>
    </form>

    <!-- Show saved data -->
    <h2>Saved data:</h2>
    <?php
    if (is_null($savedData)) {
        echo '<p>There is no saved data</p>';
    } else {
        foreach ($savedData as $data) {
            echo '<p>'.$data['name'].' : '.$data['email'].'</p>';
        }
    }
    ?>
</body>
</html>